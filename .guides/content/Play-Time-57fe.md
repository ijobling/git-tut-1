Now it’s time to play around and get used to the `git status`, `git add` and `git commit` commands. 

**IMPORTANT** : have absolutely no fear, just add files, add text to them, delete them and in between use `git status`. 

Check out what we were talking about in the previous section where we changed staged files that had not yet been committed. 

- If you want to return to your previous commit at any stage, just enter `git reset —hard HEAD`. 
If you want to go back 3 commits, use `git reset —hard HEAD~2`.
- If you want to restore everything to the initial state as of the start of this unit, enter `git reset --hard HEAD@{0}` or press the 'Restore' button in the menu at the top of the Codio IDE.

Once you feel you have completely absorbed what is going on, complete the last set of challenges starting on next section.