The Working Directory is easy to understand as it is the directory and all its sub-directories where the code you are working on lives. It’s what you see in the file tree. 

Codio projects always have a folder called ‘workspace’ (represented as `~/workspace` in the terminal window prompt) and this is the base location for your code. 

In the file tree, your workspace folder is the top most folder.

![working-dir](.guides/img/working-dir-1.png)

The above image shows the result of executing the `ls -al` command which _lists_ the visible and hidden files and directories of your file tree.

Can you identify the `.git` hidden folder in the screenshot above?

### Why do I see no files or directories in the file tree?

Files and directories that start with a dot are hidden by default (even in your Mac's Finder or your Window's File Explorer). 

The _challenges, files and final-challenges_ directories that you see in the terminal output, are not required in this section hence they are hidden from the file tree as well. You'll see them later though.