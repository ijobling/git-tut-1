There are a couple of extra things worth knowing about.

## Unstaging files
What happens if you have staged a file (using `git add`) and then change your mind and want to remove it or changes to it from the staging area?

You can unstage any file using the following command

```bash
git checkout -- file.txt
```

There are other ways we can accomplish this and similar things, which are discussed in a later unit.

## Committing and Staging simultaneously
If you Stage files and then make more edits to those files in the Working Directory and then run `git commit -m`, any changes you may have made after staging will not have been added to the commit.

There is a way simple was around this. You can commit all current changes to all *tracked* files without having to use `git add` at all. 

```bash
git commit -am 'commit message'
```

This will automatically stage any changes to all currently tracked files. However, this will not commit new files that are not yet tracked.

So, the fast and lazy way to get track/stage and commit everything is to use the following commands one after the other

```bash
git add -A
git commit -m 'commit message'
```

In the next Section, you should experiment lots so you can see exactly what Git is doing with your changes and feel completely comfortable with the entire process.