## The Basics
In this unit we will cover the basic and most commonly used Git operations.

You can navigate from one section to the next using the buttons at the top-left, the next button or by opening up the table of contents (hamburger icon top right of this window). 

## Who this course is for
This is a beginner level Course for people who have never used Git or Source Control Management systems before. The purpose is to get anyone to a level where they feel completely comfortable using Git for all common use cases. 

Total mastery of Git, which is really not necessary by the way, can then be gradually achieved using reference materials as you work with Git on a daily basis.

If you are already familiar with source control management systems then you can skip this section. 

## A fully interactive course
The great thing about this course is that it is interactive. Using Codio, you can follow the course and actually use Git at the same time. You can edit files and use the command line for Git commands with complete freedom. 

As with most things, practice makes perfect and using Codio let’s you do just that - practice as you learn. Each unit has starting points with data and commits already in place so you don’t have to tediously create them yourself. 

## Video Overview
To get an overview of what Git does, watch the video below. We’ll then move immediately into using Git.

![640x480](http://www.youtube.com/watch?v=kDUSoOubyGU&feature=youtu.be)

## Graphical Software Tools
Git is a command line tool, so you enter commands in a terminal window rather than using a pretty desktop application.

You will come across products that have a Graphical User Interface for Git. If you watched the overview video, you will have seen me using Ungit, a GUI tool that you can install on your Codio Box.

**But** - *please, please, please* don’t use them until you are comfortable with the command line. There are a few reasons for this.

- If you can use the command line, you will find GUI products much more obvious although you may decide to never use one.
- Git is a command line tool, so any Git GUI will be both an approximation and a dilution
- To get the full power of Git, the command line is as powerful as it gets
- If you find (and you will) that your GUI tool cannot do something you need to do, you will struggle as you will not be familiar with the command line
- Working with the command line is faster and more streamlined than a GUI, especially if you set up helper scripts for common commands

So - use a GUI tool later by all means - and I can recommend Ungit as a free and powerful tool - but first you must get completely comfortable with Git on the command line.