Now we are ready to *commit* files to our repo.  This will finally add all **staged** files to the repo.

With each commit, we provide a message to help us identify the commit later.

Let's commit now

```bash
git commit -m 'my first commit'
```

You’ll be rewarded with something like this

```bash
[master 16c6a81] my first commit
 3 files changed, 5 insertions(+)
 create mode 100644 files/mary.txt
 create mode 100644 files/shakepseare.txt
 create mode 100644 files/limericks.txt
```

---
Complete a set of code challenges in the next section.