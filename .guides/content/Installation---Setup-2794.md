This tutorial is running on the Codio coding platform. There’s nothing to set up, so once you are in your project, you will find Git already installed. 

If you’re NOT running this tutorial on Codio, then installing Git itself is [described here](http://git-scm.com/book/en/Getting-Started-Installing-Git).
