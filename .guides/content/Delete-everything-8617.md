[Click here to close files opened in previous section](close_file challenges/praline.txt)

Yes - we are now going to delete all of our files in the Working Directory. 

Note: After executing the command below, the file tree is going to display some of its files and directories, this is a normal behaviour as it has to recompute the changes.

Select them in the file tree and right click to delete or, if you prefer, enter the following in the terminal window:

```bash
find files/ -type f -delete
```

All your files are gone. Imagine this was a complete mistake and the panic that would ensue.

It feels a bit uncomfortable doesn’t it? However, Git let’s you recover from any earlier commit in various ways. 

We’ll recover using the simple sledgehammer approach. We’ll explain the dangers of `git reset` in a later unit.

```bash
git reset --hard HEAD
```

Now take a look at your project and the files will have been restored exactly as they were in the previous commit.

We’ll cover what this command does in more detail later on, but what it is basically saying is ‘reset the Working Directory to most recent commit (known as HEAD)'.