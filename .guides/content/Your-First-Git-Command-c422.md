Run `git status` from the terminal and you will see

```bash
# On branch master
# Your branch is up-to-date with 'origin/master'.
# 
nothing to commit, working directory clean
```

This tells you that Git cannot find anything to do.

Once you start adding or editing files in your working directory, nothing is actually added to your Git repo until you tell Git to do so. 