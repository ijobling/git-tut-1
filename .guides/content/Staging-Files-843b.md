You can think of the Staging Area as a current and temporary snapshot of files and changes that you are ready to add to your Git repo.

Take a look at the `files` directory. The only file in it is `mary.txt`. This is how things currently look to Git.

![track-stage](.guides/img/track-stage-1.png)

As soon as you run `git add`, which we'll do next, your file is:

- *Tracked* - Git will know about this file from now on
- *Staged* - Git takes a snapshot of the **current state** of the file and adds it to the *Staging Area*

So let's enter these commands in the terminal

- `git add files/mary.txt` to stage and track `mary.txt`
- `git status` again and this time you’ll see

```bash
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	new file:   files/mary.txt
```

Notice that it no longer mentions that `mary.txt` is not being tracked. It tells us that it is ready to be committed. In other words, `mary.txt` is now staged.

Now go and modify `mary.txt` again and then check the status. This time you’ll see

```bash
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

	new file:   files/mary.txt

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   files/mary.txt
```

This tells you that although `mary.txt` is being tracked and is staged, it has been modified since the last time you staged it with `git add files/mary.txt`. 

If you now commit these changes to your Git repo (coming up soon) then any changes *after* `git add files/mary.txt` would *not* be committed until you run `git add files/mary.txt` again.
