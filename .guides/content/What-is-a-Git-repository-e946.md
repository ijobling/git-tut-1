The good news is that you don’t need to know where Git stores its files and data. If you are not interested in knowing more, then skip to the next section.

# Yes, I am interested
You can think of your Git repository as a database that contains multiple snapshots in time of your code. Nothing gets added to the repository until you explicitly tell Git.

When Git is initialised in a project, a hidden directory called `.git` is created beneath your workspace folder (which is why you don’t see it in the Codio file tree but you can if you use the command line). 

So you should now be aware that the Git repo is actually a part of your project, albeit a hidden one, and is not (yet) on any external system like BitBucket or GitHub. We’ll be doing this in the next unit in fact.

If you want to see the Git repo (remember, it is just a database contained within the hidden `.git` directory), type the following from the command line (**Tools=>Terminal** menu item)

```bash
ls -al .git
```

It is not at all necessary to ever look in this folder but you can Google it for more details if you really feel the urge.
