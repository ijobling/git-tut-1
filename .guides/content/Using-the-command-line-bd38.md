When you are prompted to use the command line, a Codio terminal window is going to be enabled in the left pane.

In case you close it, you can open a new terminal window  as follows:

- From the Codio menu, select **Tools->Terminal**
- Shortcut key is **Shift+Alt+T** (all platforms)
- Go back one section and then forward to the current section to open it automatically