|||warning

Files that are created or opened explicitly by you are not automatically closed. We would recommend you close these before proceeding.

[Click here to close these files now](close_file files/mary.txt files/limerick.txt files/shakespeare.txt)

|||


{Check It!|assessment}(test-243789197)

|||guidance

### Correct answers:

1. Create the requested files: `pontius.txt` and `arthur.txt` in the `challenges` directory

2. `git add -A`

|||