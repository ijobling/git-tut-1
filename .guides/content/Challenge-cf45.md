{Check It!|assessment}(test-1297123737)


|||guidance

### Correct answers:

1. Edit the `editable.txt` and the `change-me.txt` file adding or removing any text or character
2. `git add -A`
3. `git commit -m "Edited challenge files"`

|||