[Click here to close files opened in previous section](close_file challenges/pontius.txt challenges/arthur.txt)

{Check It!|assessment}(test-2052257489)


---

Your files are now safely stored in the Git repo and you would have to try very hard to lose that code, which is exactly what we’ll try to do next.

|||guidance

### Correct answers:

1. Create the requested file: `praline.txt` in the `challenges` directory
2. `git add challenges/praline.txt`
3. `git commit -m "Added praline.txt file"`

|||