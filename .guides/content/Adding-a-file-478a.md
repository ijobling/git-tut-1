So, let's do something

1. Add a new file `mary.txt` to the `files` directory we created for you (right click the `files` folder in the file tree and select New File from the pop-up menu)
2. Copy and paste the following text into your new file

```bash
Mary had a little lamb,
His fleece was white as snow,
And everywhere that Mary went,
The lamb was sure to go.
```
3. Enter `git status` in the terminal

You should now see this:

```bash
# On branch master
# Your branch is up-to-date with 'origin/master'. 
#                                                                                                  
# Untracked files:
#   (use "git add <file>..." to include in what will be committed) 
#                                                                                                  
#       files/  
#                                                                                                  
nothing added to commit but untracked files present (use "git add" to track)
```

This tells you the following

- you are on the `master` branch (more on that later)
- Git has detected that files inside the `files/` directory are not yet tracked by Git (for instance the `mary.txt` file), which we'll deal with in the next section.

---
Time to complete your first set of challenges. 

Go to the next section.