Let’s make some more changes.

- add another 2 files to the `files` folder (maybe call them `shakespeare.txt` and `limerick.txt`) and add a short bit of content to each file. 
- make a small change to the contents of `mary.txt` (anything at all)
- run `git status` from the command line

```bash
# On branch master
# Changes to be committed:
#   (use "git reset HEAD <file>..." to unstage)
#
#       new file:   files/mary.txt
#
# Changes not staged for commit:
#   (use "git checkout -- <file>..." to discard changes in working directory)  
#
#       modified:   files/mary.txt
#
# Untracked files: 
#   (use "git add <file>..." to include in what will be committed) 
# 
#       files/limerick.txt
#       files/shakespeare.txt
#  
```

Git tells you that the new files are **not** being tracked. So things look like this now.

![](.guides/img/track-stage-3.png)

If we want to add these new files to our repo, we will need to stage them.

Luckily, there is a quick way to add all untracked files (and all unstaged edits in all tracked files) to the staging area. Just enter the following

```
git add -A
```

Run `git status` again

```
# On branch master
# Changes to be committed:
#   (use "git reset HEAD <file>..." to unstage)
#
#       new file:   files/limerick.txt
#       new file:   files/mary.txt
#       new file:   files/shakespeare.txt
#  
```

You can see how everything is nicely staged, ready to commit to our Git repository.

So we finally have this

![](.guides/img/track-stage-4.png)

**IMPORTANT** : If you have made any further edits to the files,  be sure to stage them with `git add` first.