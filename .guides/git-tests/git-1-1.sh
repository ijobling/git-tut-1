#!/bin/bash
QCOUNT=3

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
			1 )
				expect "Stage the requested files" "Changes to be committed" "git status"
				;;
			2 )
				expect "'pontius.txt' file is not staged for commit" "new file:[[:space:]]*challenges/pontius.txt" "git status"
				;;
			3 )
				expect "'arthur.txt' file is not staged for commit" "new file:[[:space:]]*challenges/arthur.txt" "git status"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command