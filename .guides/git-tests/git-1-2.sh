#!/bin/bash
QCOUNT=2

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
			1 )
				expect "'praline.txt' file hasn't been committed yet" "diff --git a/challenges/praline.txt b/challenges/praline.txt" "git log -p -1"
				;;
			2 )
				expect "Use the requested commit message" "Added praline.txt file" "git log -p -1"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command