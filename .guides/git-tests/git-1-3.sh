#!/bin/bash
QCOUNT=3

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
			1 )
				expect "Commit the requested files" "diff --git a/final-challenges/editable.txt b/final-challenges/editable.txt" "git log -p -1"
				;;
      2 )
				expect "Commit the requested files" "diff --git a/final-challenges/change-me.txt b/final-challenges/change-me.txt" "git log -p -1"
				;;
			3 )
				expect "Use the requested commit message" "Edited challenge files" "git log -p -1"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command